import math

#largeur des bandes + Prix 4g /5g
d={"700 MHz":(5,7000,8000),
   "800 MHz":(10,10000,12000),
   "1800 MHz":(20,14000,14000),
   "2100 MHz":(15,12000,13500),
   "2600 MHz":(15,12000,13500),
   "3500 MHz":(0,+math.inf,35000)
}

config_initiale={"700 MHz":False,
   "800 MHz":True,
   "1800 MHz":True,
   "2100 MHz":True,
   "2600 MHz":False,
   "3500 MHz":False
}

bandes=["700 MHz",
   "800 MHz",
   "1800 MHz",
   "2100 MHz",
   "2600 MHz",
   "3500 MHz"]


#dimensionne un secteur 4G en indiquant le trafic max a atteindre et la configuration initiale des bandes
def dimensionnement_4G(traficMax,config_initiale):
    budget=math.inf
    trafic=None
    configOpt=None
    for i in range (2**6):
        order=format(i,'06b')
        config=setconfig(config_initiale,order)
        t=0
        c=0
        for bands in config:
            if config[bands]:
                t=t+d[bands][0]*1.58
                if not config_initiale[bands]:
                    c=c+d[bands][1]
        if (t>=traficMax) and (c<budget):
            trafic=t
            budget=c
            configOpt=config

    return(budget,trafic, configOpt)

#dimensionne un secteur 5G en indiquant le trafic max a atteindre et la configuration initiale des bandes
def dimensionnement_5G(traficMax_5G,config_initiale_5G,trafic_max_4G,config_initiale_4G):
    budget=math.inf
    trafic=None
    configOpt=None
    budget,trafic,configOpt=get_best_config(traficMax_5G,config_initiale_5G,1)

    config_4G=config_initiale_4G.copy()
    
    for i in range (2**6):
        order=format(i,'06b')
        config=setconfig(config_initiale,order)
        c=0
        t=0
        check=False
        new_config_4G=config_initiale_4G.copy()
        new_config_5G=config_initiale_5G.copy()
        l=[]

        for band in config:
            if (config_initiale_4G[band]) and (config[band]):
                c+=2000
                new_config_4G[band]=False
                new_config_5G[band]=True
                check=True
                l.append(band)
        if check :
            budget_4G,trafic_4G,configOpt_4G=get_best_config(trafic_max_4G,new_config_4G,0)
            budget_5G,trafic_5G,configOpt_5G=get_best_config(traficMax_5G,new_config_5G,1)
            cost_total=c+budget_4G+budget_5G
            if (cost_total<budget) and(trafic_4G!=None):

                trafic=trafic_5G
                budget=cost_total
                configOpt=configOpt_5G
                config_4G=configOpt_4G
                lfin=l
        
#lfin sont les bandes de frequences à faire evoluer du 4G au 5G
    return (budget,trafic, configOpt,config_4G,lfin)
            

def get_best_config(traficMax,config_initiale,val):
    budget=math.inf
    trafic=None
    configOpt=None
    const=val*0.2+1
    for i in range (2**6):
        order=format(i,'06b')
        config=setconfig(config_initiale,order)
        t=0
        c=0
        for bands in config:
            if config[bands]:
                t=t+d[bands][0]*1.59*const
                if not config_initiale[bands]:
                    c=c+d[bands][1+val]
        if (t>=traficMax) and (c<budget):
            trafic=t
            budget=c
            configOpt=config

    return(budget,trafic, configOpt)


def setconfig(config_initiale, order):
    dico=config_initiale.copy()
    for i in range(len(order)):
        if order[i]=="1" :
            dico[bandes[i]]=True
    return(dico)
    

print(dimensionnement_4G(150,config_initiale))
